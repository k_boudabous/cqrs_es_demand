# TP SCALA EVENT SOURCING & CQRS : cqrs_es_demand


## Docker

Lancer le docker-compose, à partir de la racine du projet, pour lancer Zookeeper, Kafka et MongoDB :

`docker-compose up`


## Run API Scala

Dans une console, à la racine du projet : 

`sbt run`


## Use API Scala

Lister les demandes [GET] : http://localhost:9000/demands

Accéder à une demande [GET] : http://localhost:9000/demands/:number

Créer une demande [POST] : http://localhost:9000/demands

`{`
	`"number":"1",`
	`"label":"test",`
	`"quantity":1`
`}`

Modifer une demande [PUT] : http://localhost:9000/demands/:number

`{`
	`"number":"1",`
	`"label":"test",`
	`"quantity":3`
`}`

## Kafka (cmd Windows)

Lister les topics :

`winpty docker exec -ti cqrs_es_demand_kafka_1 opt/kafka/bin/kafka-topics.sh --list --zookeeper zookeeper:2181`

Lire les messages d'un topic :

`winpty docker exec -ti cqrs_es_demand_kafka_1 opt/kafka/bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic demandTopic --from-beginning`


## MongoDB (cmd Windows)

Accéder à la base de données `demand` et lire les données de la collection `demands` :

`winpty docker exec -it mongodb bash`

`mongo`

`use demand`

`db.demands.find()`