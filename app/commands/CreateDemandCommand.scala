package commands

import events.DemandEvent
import javax.inject.Inject
import kafka.ProducerDemand
import models.Demand
import services.DemandServiceImpl

import scala.concurrent.ExecutionContext

class CreateDemandCommand @Inject()(implicit ec: ExecutionContext, demandServiceImpl: DemandServiceImpl) extends Command[Demand] {
  val demandEvent = new DemandEvent

  override def execute(demand: Demand): Demand = {


    demandServiceImpl.save(demand).map {
      _ => ProducerDemand.sendToKafka(demandEvent.createdDemandEvent(demand))
    }
    return demand
  }
}