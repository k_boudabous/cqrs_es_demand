package commands


import events.DemandEvent
import javax.inject.Inject
import kafka.ProducerDemand
import models.Demand
import services.DemandServiceImpl

import scala.concurrent.ExecutionContext

class UpdateDemandCommand @Inject()(implicit ec: ExecutionContext, demandServiceImpl: DemandServiceImpl) extends Command[Demand] {
  val demandEvent = new DemandEvent

  override def execute(modifiedDemand: Demand): Demand = {

    demandServiceImpl.find(modifiedDemand.number).map {
      case Some(demand) => demandServiceImpl.update(modifiedDemand.number, modifiedDemand).map {
        _ => ProducerDemand.sendToKafka(demandEvent.updatedDemandEvent(modifiedDemand, demand, "ModifiedDemand"))
      }
      case _ => null
    }

    return modifiedDemand
  }


}