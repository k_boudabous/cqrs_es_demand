package controllers

import commands.{CreateDemandCommand, UpdateDemandCommand}
import javax.inject.Inject
import models.Demand
import play.api.libs.json._
import play.api.mvc.{AbstractController, ControllerComponents}
import services.DemandServiceImpl

import scala.concurrent.{ExecutionContext, Future}

class DemandController @Inject()(implicit ec: ExecutionContext, cc: ControllerComponents, demandServiceImpl: DemandServiceImpl, updateDemandCommand: UpdateDemandCommand, createDemandCommand: CreateDemandCommand) extends AbstractController(cc) {

  def index = Action.async {
    demandServiceImpl.findAll().map(demands => Ok(Json.toJson(demands)))
  }

  def read(number: String) = Action.async {
    demandServiceImpl.find(number).map { demand =>
      demand.map { demand =>
        Ok(Json.toJson(demand))
      }.getOrElse(NotFound("NOT_FOUND"))
    }
  }

  def create = Action.async(parse.json) {
    _.body.validate[Demand]
      .map {
        demand =>
          createDemandCommand.execute(demand)
          Future.successful(Ok(Json.toJson(demand)))
      }.getOrElse(Future.successful(BadRequest("INVALID_FORMAT")))
  }

  def update(number: String) = Action.async(parse.json) {
    _.body.validate[Demand].map {
      demand =>
        updateDemandCommand.execute(demand)
        Future.successful(Ok(Json.toJson(demand)))
    }.getOrElse(Future.successful(BadRequest("INVALID_FORMAT")))
  }
}