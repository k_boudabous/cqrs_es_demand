package events

import models.Demand
import net.liftweb.json.Serialization.write
import net.liftweb.json._


case class Event(name: String, demand_id: String, payload: String)

class DemandEvent {


  def createdDemandEvent(demand: Demand): String = {
    implicit val formats = DefaultFormats
    val payload = Map("number" -> demand.number, "label" -> demand.label, "quantity" -> demand.quantity)
    val e = Event("CreatedDemand", demand.number, write(payload))
    return write(e)
  }

  def updatedDemandEvent(demand: Demand, context: Demand, command: String): String = {
    implicit val formats = DefaultFormats

    val originalSet = Map("number" -> context.number, "label" -> context.label, "quantity" -> context.quantity)
    val modifiedSet = Map("number" -> demand.number, "label" -> demand.label, "quantity" -> demand.quantity)

    val jsonPayload = write((modifiedSet.toSet diff originalSet.toSet).toMap)
    val e = Event(command, demand.number, jsonPayload)
    return write(e)
  }
}