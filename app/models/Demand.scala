package models

import play.api.libs.json.{OFormat}
import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Writes, _}
import reactivemongo.bson.BSONObjectID
import reactivemongo.play.json._
case class Demand(
                   _id: Option[BSONObjectID],
                   number: String,
                   label: String,
                   quantity: Int
                 )

object Demand {
  val demandWrites: Writes[Demand] = (
    (JsPath \ "_id").writeNullable[BSONObjectID] and
      (JsPath \ "number").write[String] and
      (JsPath \ "label").write[String] and
      (JsPath \ "quantity").write[Int]
    ) (unlift(Demand.unapply))

  implicit val demandReads: Reads[Demand] = (
    (JsPath \ "_id").readNullable[BSONObjectID] and
      (JsPath \ "number").read[String] and
      (JsPath \ "label").read[String] and
      (JsPath \ "quantity").read[Int]
    ) (Demand.apply _)

  implicit val demandFormat: Format[Demand] = Format(demandReads, demandWrites)

  implicit val demandOFormat = new OFormat[Demand] {
    override def reads(json: JsValue): JsResult[Demand] = demandFormat.reads(json)
    override def writes(o: Demand): JsObject = demandFormat.writes(o).asInstanceOf[JsObject]
  }
}