package repositories

import models.Demand
import reactivemongo.api.commands.WriteResult
import scala.concurrent.{ExecutionContext, Future}

trait DemandRepository {

  def findAll(limit: Int)(implicit ec: ExecutionContext): Future[Seq[Demand]]

  def findByDemandNumber(number: String)(implicit ec: ExecutionContext): Future[Option[Demand]]

  def save(demand: Demand)(implicit ec: ExecutionContext): Future[WriteResult]

  def update(number: String, demand: Demand)(implicit ec: ExecutionContext): Future[Option[Demand]]

}