package repositories

import javax.inject.Inject
import models.Demand
import play.modules.reactivemongo.ReactiveMongoApi
import reactivemongo.api.commands.WriteResult
import reactivemongo.api.{Cursor, ReadPreference}
import reactivemongo.bson.{BSONDocument, BSONObjectID}
import reactivemongo.play.json._
import reactivemongo.play.json.collection.JSONCollection
import scala.concurrent.{ExecutionContext, Future}

class DemandRepositoryImpl @Inject()(implicit ec: ExecutionContext, reactiveMongoApi: ReactiveMongoApi) extends DemandRepository {

  override def findAll(limit: Int = 100)(implicit ec: ExecutionContext): Future[Seq[Demand]] =
    collection.flatMap(_.find(BSONDocument()).cursor[Demand](ReadPreference.primary).collect[Seq](limit, Cursor.FailOnError[Seq[Demand]]()))

  override def findByDemandNumber(number: String)(implicit ec: ExecutionContext): Future[Option[Demand]] =
    collection.flatMap(_.find(BSONDocument("number" -> number)).one[Demand])

  override def save(demand: Demand)(implicit ec: ExecutionContext): Future[WriteResult] =
    collection.flatMap(_.insert(demand))

  private def collection: Future[JSONCollection] = reactiveMongoApi.database.map(_.collection("demands"))

  override def update(number: String, demand: Demand)(implicit ec: ExecutionContext): Future[Option[Demand]] =
    collection.flatMap(_.findAndUpdate(BSONDocument("number" -> number), BSONDocument(
      f"$$set" -> BSONDocument(
        "number" -> demand.number,
        "label" -> demand.label,
        "quantity" -> demand.quantity
      )
    ),
      true
    ).map(_.result[Demand])
    )
}