package services

import javax.inject.Inject
import models.Demand
import reactivemongo.api.commands.WriteResult
import repositories.DemandRepositoryImpl
import scala.concurrent.{ExecutionContext, Future}

class DemandServiceImpl @Inject()(implicit ec: ExecutionContext, demandRepository: DemandRepositoryImpl) {

  def findAll(): Future[Seq[Demand]] =
    demandRepository.findAll()

  def find(number: String): Future[Option[Demand]] =
    demandRepository.findByDemandNumber(number = number)

  def save(demand: Demand): Future[WriteResult] = {
    demandRepository.save(demand)
  }

  def update(number: String, demand: Demand): Future[Option[Demand]] =
    demandRepository.update(number = number, demand = demand)
}